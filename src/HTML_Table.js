import React, { Component } from 'react';
import { Table, Button, Icon, Popup, Label, Image } from 'semantic-ui-react';
import HTML_Labels from './HTML_Labels';
import {
    round
} from './Parse';

export default class HTML_Table extends Component {

    constructor(props) {
        super(props);

        this.state = {
            state: this.props.state,
            column1: null,
            column2: null,
            direction: null
        }
    }

    componentDidMount() {
        this.handleSort('stocks_present', 'total_invested_EUR')();
    }

    handleSort = (data, item, letter) => () => { // this is a listener, we need to return a function, otherwise I assume it'd be executed immediately
        const { state, column1, column2, direction } = this.state;

        /* objects are ordered in ES6
        First all Array indices, sorted numerically.
        Then all string keys (that are not indices), in the order in which they were created.
        Then all symbols, in the order in which they were created.
        */

        const get_sorted = (obj, item, firstTime) => {
            return Object.keys(obj).sort((a, b) => {
                if (firstTime)
                    return obj[a][item] - obj[b][item];
                else
                    return direction === 'ascending' ? obj[b][item] - obj[a][item] : obj[a][item] - obj[b][item];
            }).reduce((result, key) => {
                result[key] = obj[key];
                return result;
            }, {});
        };

        const sort_alphabetically = (a, b, reverse) => {
            let value = -1;
            if (reverse)
                value = 1;
            if (a < b) return value;
            else if (a > b) return -value;
            return 0;
        }

        const get_letterSorted = (obj, item, firstTime) => {
            return Object.keys(obj).sort((a, b) => {
                if (firstTime) {
                    return sort_alphabetically(obj[a][item], obj[b][item]);
                } else
                    if (direction === 'ascending') {
                        return sort_alphabetically(obj[a][item], obj[b][item], 'reverse');
                    } else {
                        return sort_alphabetically(obj[a][item], obj[b][item]);
                    }
            }).reduce((result, key) => {
                result[key] = obj[key];
                return result;
            }, {});
        };

        if (data === 'stocks_present' && column1 !== item
            || data === 'stocks_past' && column2 !== item) {
            if (letter)
                state[data] = get_letterSorted(state[data], item, 'first time');
            else
                state[data] = get_sorted(state[data], item, 'first time');
        }

        if (data === 'stocks_present' && column1 !== item) {
            this.setState({
                stocks: data,
                column1: item,
                state: state,
                direction: 'ascending'
            });
            return;
        }

        if (data === 'stocks_past' && column2 !== item) {
            this.setState({
                stocks: data,
                column2: item,
                state: state,
                direction: 'ascending'
            });
            return;
        }

        if (letter)
            state[data] = get_letterSorted(state[data], item);
        else
            state[data] = get_sorted(state[data], item);

        this.setState({
            state: state,
            direction: direction === 'ascending' ? 'descending' : 'ascending',
        })
    }


    render() {
        const { state, column1, column2, direction } = this.state;
        const IndividualValue = (a, b, c) => <Popup trigger={<span>{a} {b}</span>} size='mini' content={c} />;
        const get_stock_price = (item) => item.stock_price ? item.stock_price + ' ' + item.coin + ' [' + round(item.stock_price / item.EUR_exchange) + ' EUR]' : '..';
        const open_links = (item) => () => {
            let links = ['https://www.google.com/search?q=' + item.name,
            'https://finance.yahoo.com/quote/' + item.ticker,
            'https://twitter.com/search?f=tweets&vertical=default&q=$' + item.ticker,
            'https://twitter.com/search?f=tweets&vertical=default&q=' + item.name,
            'https://stocktwits.com/symbol/' + item.ticker,
            'https://finbox.io/' + item.ticker,
            'https://finviz.com/quote.ashx?t=' + item.ticker,
            'https://research.valueline.com/research#list=recent&sec=company&sym=' + item.ticker,
            'https://www.marketscreener.com/' + item.name,
            'https://www.tipranks.com/stocks/' + item.ticker,
            'https://www.gurufocus.com/financials/' + item.ticker,
            'https://marketrealist.com/quote-page/' + item.ticker,
            'https://stockinvest.us/technical-analysis/' + item.ticker,
            'https://www.morningstar.com/stocks/xnys/' + item.ticker,
            'https://www.tipranks.com/stocks/' + item.ticker,
            'https://www.thestreet.com/find/results?q=' + item.name,
            'https://fintel.io/s/us/' + item.ticker,
            'https://seekingalpha.com/symbol/' + item.ticker,
            'https://unicornbay.com/t/' + item.ticker,
            'https://wallmine.com/' + item.ticker,
            'https://www.insidermonkey.com/insider-trading/company/' + item.name];
            links.map(url => window.open(url, "_blank"));
        }

        const get_popup = (name, content, label) => {
            let basic = true;
            if (label === 'greyed') basic = false;
            return (
                <Popup
                    trigger={<Label basic={basic}>{name}</Label>}
                    content={content}
                    position='top right'
                    size='mini'
                />
            )
        };

        return (
            <div>
                <div className='myreact container'>
                    <Table className='myreact tableHeader' size='small' padded='very' striped sortable celled >
                        <Table.Header fullWidth>
                            <Table.Row className='myreact tableHeader_1'>
                                <Table.HeaderCell>{IndividualValue('TODAY', '', 'Value normalized from a 100 EUR amount')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Net Income', '', 'Money moved to the account')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Cash', '', 'parent.net_income + parent.pool.total_net_invested_EUR')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Net Gains', '', 'amount if we sell today, including stock and global fees and dividends')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Total Raw Invested', '', 'amount spent without any type of fees and dividends')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Total Net Invested', '', 'amount spent with fees and dividends')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Global Fees', '', '')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Stock Fees', '', '')}</Table.HeaderCell>
                                <Table.HeaderCell>{IndividualValue('Stock Dividends', '', '')}</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            <Table.Row className='myreact tableHeader_2'>
                                <Table.Cell>{IndividualValue(state.pool.performance, 'EUR', 'state.net_income')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.net_income, 'EUR', 'state.net_income')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.pool.cash, 'EUR', 'state.pool.cash')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.pool.total_value_net_eur, 'EUR', 'state.pool.total_value_net_eur')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.pool.total_invested_EUR, 'EUR', 'state.pool.total_invested_EUR')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.pool.total_net_invested_EUR, 'EUR', 'state.pool.total_net_invested_EUR')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.global_fees, 'EUR', 'state.global_fees')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.stock_fees, 'EUR', 'state.stock_fees')}</Table.Cell>
                                <Table.Cell>{IndividualValue(state.stock_dividends, 'EUR', 'state.stock_dividends')}</Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </div>
                <div className='myreact container'>
                    <Table className='myreact table' compact='very' size='small' selectable striped sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell className="non_sortable"></Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'name' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'name', 'alphabetically')}
                                >
                                    {get_popup('CURRENT Stocks', 'Stocks we currently own')}
                                    {column1 !== 'name' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Ticker', 'Yahoo symbol', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Today Price', 'Price extracted from Yahoo Finance', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'stock_change' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'stock_change')}
                                >
                                    {get_popup('Today Change', 'Price change in percentage, extracted from Yahoo Finance')}
                                    {column1 !== 'stock_change' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Spent', 'Money spent in original coin without fees', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'total_invested_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'total_invested_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Money spent in EUR with the exchange of that moment')}
                                    {column1 !== 'total_invested_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Recovered', 'Money recovered from selling stocks, in original coin', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'total_recovered_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'total_recovered_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Money recovered in EUR with the exchange of that moment')}
                                    {column1 !== 'total_recovered_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('TODAY RAW Value', 'Today value of our stocks in original coin (without fees & dividends)', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('[ % ]', 'Percentage of gains in original coin (without fees & dividends)', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'value_invested_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'value_invested_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Today value of our stocks in EUR (without fees & dividends)')}
                                    {column1 !== 'value_invested_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'diff_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'diff_EUR')}
                                >
                                    {get_popup('[ % ]', 'Percentage of gains in EUR (without fees & dividends)')}
                                    {column1 !== 'diff_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'value_invested_NET_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'value_invested_NET_EUR')}
                                >
                                    {get_popup('TODAY NET Value [ EUR ]', 'Today value of our stocks in EUR and with fees & dividends')}
                                    {column1 !== 'value_invested_NET_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'diff_NET_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'diff_NET_EUR')}
                                >
                                    {get_popup('[ % ]', 'Percentage of gains in EUR with fees & dividends')}
                                    {column1 !== 'diff_NET_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'NET_total_wins' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'NET_total_wins')}
                                >
                                    {get_popup('TOTAL (net) RESULT [ EUR ]', 'The total gains if we sell today, including fees & dividends')}
                                    {column1 !== 'NET_total_wins' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'NET_remaining_wins' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'NET_remaining_wins')}
                                >
                                    {get_popup('REMAINING (raw) RESULTS [ EUR ]', 'The total gains if we sell today, but only accounting for the stocks'
                                        + ' we own NOW, not including the stocks we had and we already sold in the past, and without fees & dividends')}
                                    {column1 !== 'NET_remaining_wins' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Stocks', 'Number of stocks we own', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Operations', 'Click to see a summary of every activity we have made', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Avr. price bought', 'Of the stocks we own, the average price we have paid for them', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Dividends', 'Sum of dividends received', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'total_dividends_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'total_dividends_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Sum of dividends received in EUR (exchange of that moment)')}
                                    {column1 !== 'total_dividends_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column1 === 'total_fees' ? direction : null}
                                    onClick={this.handleSort('stocks_present', 'total_fees')}
                                >
                                    {get_popup('Fees [ EUR ]', 'Sum of fees paid')}
                                    {column1 !== 'total_fees' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {Object.keys(state.stocks_present).map(key => {
                                let item = state.stocks_present[key];
                                return (
                                    <Table.Row key={item.stock_unique}>
                                        <Table.Cell>
                                            <Button size='mini' icon onClick={open_links(item)}><Icon name='folder outline' /></Button>
                                        </Table.Cell>
                                        <Table.Cell className={item.total_invested_EUR < -600 ? 'main_investment' : ''}>{IndividualValue(item.name, '', 'item.name')}</Table.Cell>
                                        <Table.Cell>{item.ticker} {item.market_open ? <Icon name='chart line' /> : ''}</Table.Cell>
                                        <Table.Cell>{get_stock_price(item)}</Table.Cell>
                                        <Table.Cell className={item.stock_change > 3 ? 'good_change' : item.stock_change < -3 ? 'bad_change' : ''}>{IndividualValue(item.stock_change, '%', 'item.stock_change')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_invested, item.coin, 'item.total_invested')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_invested_EUR, 'EUR', 'item.total_invested_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_recovered, item.coin, 'item.total_recovered')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_recovered_EUR, 'EUR', 'item.total_recovered_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.value_invested, item.coin, 'item.value_invested')}</Table.Cell>
                                        <Table.Cell>{IndividualValue('[ ' + item.diff + '% ]', '', 'item.diff')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.value_invested_EUR, 'EUR', 'item.value_invested_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue('[ ' + item.diff_EUR + '% ]', '', 'item.diff_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.value_invested_NET_EUR, 'EUR', 'item.value_invested_NET_EUR')}</Table.Cell>

                                        <Table.Cell className={item.diff_NET_EUR > 20 ? 'good_inv_change' : item.diff_NET_EUR < -20 ? 'bad_inv_change' : ''}>{IndividualValue(item.diff_NET_EUR + '%', '', 'item.diff_NET_EUR')}</Table.Cell>
                                        <Table.Cell className={item.NET_total_wins > 300 ? 'good_inv_change' : item.NET_total_wins < -300 ? 'bad_inv_change' : ''}>{IndividualValue(item.NET_total_wins, 'EUR', 'item.NET_total_wins')}</Table.Cell>

                                        <Table.Cell>{IndividualValue(item.NET_remaining_wins + ' EUR [ ' + (-item.NET_remaining_wins_diff) + '% ]', '', 'item.NET_remaining_wins (-item.NET_remaining_wins_diff)')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_num_stocks, '', 'item.total_num_stocks')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(<HTML_Labels events={item.events} />, '', 'item.events')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.average_bought_price_per_stock, item.coin, 'item.average_bought_price_per_stock')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_dividends, item.dividends_coin, 'item.total_dividends')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_dividends_EUR, 'EUR', 'item.total_dividends_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_fees, 'EUR', 'item.total_fees')}</Table.Cell>

                                    </Table.Row>
                                );
                            })}
                        </Table.Body>
                    </Table>
                </div>
                <div className='myreact container'>
                    <Table className='myreact table' compact='very' size='small' selectable striped sortable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell className="non_sortable"></Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'name' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'name', 'alphabetically')}
                                >
                                    {get_popup('PRIOR Stocks', 'Stocks we have owned and already finished', 'grey')}
                                    {column2 !== 'name' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Ticker', 'Yahoo symbol', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Today Price', 'Price extracted from Yahoo Finance', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'stock_change' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'stock_change')}
                                >
                                    {get_popup('Today Change', 'Price change in percentage, extracted from Yahoo Finance')}
                                    {column2 !== 'stock_change' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Spent', 'Money spent in original coin', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'total_invested_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'total_invested_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Money spent in EUR with the exchange of that moment')}
                                    {column2 !== 'total_invested_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Raw Gain', 'The difference between the spent and the recovered, in original coin', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('... [ % ]', 'Percentage of the final gains, in original coin', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'total_net_gain_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'total_net_gain_EUR')}
                                >
                                    {get_popup('Net Gain [ EUR ]', 'The difference between the spent and the recovered, including fees and dividends, in EUR with the exchange of that moment')}
                                    {column2 !== 'total_net_gain_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'total_net_gain_EUR_percentage' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'total_net_gain_EUR_percentage')}
                                >
                                    {get_popup('... [ % ]', 'Percentage of the final net gains, in EUR')}
                                    {column2 !== 'total_net_gain_EUR_percentage' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Operations', 'Click to see a summary of every activity we have made', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Avr. price bought', 'Of the total stocks, the average price we have paid for them', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Avr. price sold', 'Of the total stocks, the average price for which we have sold them', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell className="non_sortable">{get_popup('Dividends', 'Sum of dividends received', 'greyed')}</Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'total_dividends_EUR' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'total_dividends_EUR')}
                                >
                                    {get_popup('... [ EUR ]', 'Sum of dividends received in EUR (exchange of that moment)')}
                                    {column2 !== 'total_dividends_EUR' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                                <Table.HeaderCell
                                    sorted={column2 === 'total_fees' ? direction : null}
                                    onClick={this.handleSort('stocks_past', 'total_fees')}
                                >
                                    {get_popup('Fees [ EUR ]', 'Sum of fees paid')}
                                    {column2 !== 'total_fees' ? <Icon name='sort' /> : null}
                                </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>
                            {Object.keys(state.stocks_past).map(key => {
                                let item = state.stocks_past[key];
                                return (
                                    <Table.Row key={item.stock_unique}>
                                        <Table.Cell>
                                            <Button size='mini' icon onClick={open_links(item)}><Icon name='folder outline' /></Button>
                                        </Table.Cell>
                                        <Table.Cell className={item.total_invested_EUR < -600 ? 'main_investment' : ''}>{IndividualValue(item.name, '', 'item.name')}</Table.Cell>
                                        <Table.Cell>{item.ticker} {item.market_open ? <Icon name='chart line' /> : ''}</Table.Cell>
                                        <Table.Cell>{get_stock_price(item)}</Table.Cell>

                                        <Table.Cell className={item.stock_change > 3 ? 'good_change' : item.stock_change < -3 ? 'bad_change' : ''}>{IndividualValue(item.stock_change + '%', '', 'item.stock_change')}</Table.Cell>

                                        <Table.Cell>{IndividualValue(item.total_invested, item.coin, 'item.total_invested')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_invested_EUR, 'EUR', 'item.total_invested_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_gain, item.coin, 'item.total_gain')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_gain_percentage + '%', '', 'item.total_gain_percentage')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_net_gain_EUR, 'EUR', 'item.total_net_gain_EUR')}</Table.Cell>

                                        <Table.Cell className={item.total_net_gain_EUR_percentage > 20 ? 'good_inv_change' : item.total_net_gain_EUR_percentage < -20 ? 'bad_inv_change' : ''}>{IndividualValue(item.total_net_gain_EUR_percentage + '%', '', 'item.total_net_gain_EUR_percentage')}</Table.Cell>

                                        <Table.Cell>{IndividualValue(<HTML_Labels events={item.events} />, '', 'item.events')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.average_bought_price_per_stock, item.coin, 'item.average_bought_price_per_stock')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.average_sold_price_per_stock, item.coin, 'item.average_sold_price_per_stock')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_dividends, item.dividends_coin, 'item.total_dividends')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_dividends_EUR, 'EUR', 'item.total_dividends_EUR')}</Table.Cell>
                                        <Table.Cell>{IndividualValue(item.total_fees, 'EUR', 'item.total_fees')}</Table.Cell>

                                    </Table.Row>
                                );
                            })}
                        </Table.Body>
                    </Table>
                </div>
            </div>
        );
    }
}
